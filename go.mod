module gitee.com/quant1x/gotdx

go 1.20

require (
	github.com/mymmsc/gox v1.3.1
	github.com/stretchr/testify v1.8.1
	golang.org/x/text v0.5.0
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

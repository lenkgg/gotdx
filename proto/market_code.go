package proto

type Market = uint8

const (
	// 深圳
	MarketShenZhen Market = iota
	// 上海
	MarketShangHai Market = 1
	// 北京
	MarketBeiJing Market = 2
)
